#include "all.h"

int loglevel = 1;

void
logprintf(int loglvl, const char *fmt, ...) {
  if (loglevel < loglvl)
    return;
  if (loglevel > 1) {
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
    fprintf(stderr, "%ld.%09ld ", now.tv_sec, now.tv_nsec);
  }
	va_list ap;
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
}

void
logfatal(int exitcode, int loglvl, const char *fmt, ...) {
	va_list ap;
	va_start(ap, fmt);
	logprintf(loglvl, fmt, ap);
	va_end(ap);
  exit(exitcode);
}
