struct timespec ts_min(struct timespec a, struct timespec b);
struct timespec ts_plus(struct timespec a, struct timespec b);
struct timespec ts_minus(struct timespec a, struct timespec b);
void ts_add_ms(struct timespec *a, long ms);
void ts_add_ms_p(struct timespec *a, long ms);
