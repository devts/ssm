typedef struct {
  char *cname;
  int (*cf)(char *);
} cmd;
cmd *cmds;
size_t ncmds;

int ssm_load(char *name);
int cmd_load(char *name);
int ssm_reload(char *name);
int cmd_reload(char *name);
int ssm_start(char *name);
int cmd_start(char *name);
int ssm_stop(char *name);
int cmd_stop(char *name);
int ssm_restart(char *name);
int cmd_restart(char *name);
int ssm_term(void);
int cmd_term(char *ign);
int cmd_status(char *name);
int cmd_list(char *ign);

void exec_cmd(char *cmd, size_t ncmd);
