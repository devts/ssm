#include "all.h"

int
sigsethandler(int sig, void (*f)(int)) {
  struct sigaction action = {.sa_handler = f, .sa_flags = SA_RESTART | SA_NOCLDSTOP};
  sigfillset(&action.sa_mask);
  return sigaction(sig, &action, 0);
}
