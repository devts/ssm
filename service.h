enum {
  WAIT_DEPS = 0,
  WAIT_TIMEOUT,
  UNAVAILABLE,
  AVAILABLE,
  STOPPING,
  FINISH,
  STOPPED,
  FAILED,
};

typedef struct service service;
struct service {
  /* config */
  char *name;
  char *path;
  int daemon;

  char **dep_names;
  size_t ndep_names;
  size_t mdep_names;

  service **deps;
  size_t ndeps;
  size_t mdeps;

  char *run_exe;
  char **run_argv;
  size_t run_nargv;
  size_t run_margv;

  char *fin_exe;
  char **fin_argv;
  size_t fin_nargv;
  size_t fin_margv;

  char *in;
  char *out;

  int notif_fd;
  long timeout_kill; /* timeout between TERM and KILL in ms */
  long timeout_finish; /* timeout from start to KILL in ms */
  long timeout_restart; /* restart timeout after failure / crash of run */

  /* runtime */
  int notif_fd_src;
  int out_fd;
  pid_t pid;
  int status;
  int target;
  struct timespec not_before;
  /* runtime log */
  struct timespec last_run_start;
  struct timespec last_run_stop;
  int last_run_exit_status;
  struct timespec last_available_start;
  struct timespec last_available_stop;
};

service *service_new(void);
void service_delete(service *sv, int closefds);
service *service_copy(service *sv);
void service_ser(service *sv, char **buf, size_t *nbuf, size_t *mbuf);
int service_deser(char *buf, size_t nbuf, size_t *of, service **svr);
void service_reset_cfg(service *sv);
int service_read_cfg(service *sv);
service *service_read(char *path, char *name);
char *service_status_str(int status);
void service_print_info(service *sv);
int service_transition_exec_run(service *sv);
int service_transition_exec_fin(service *sv);
int service_transition_notified(service *sv);
int service_fd_polled(service *sv, int revents);
int service_transition_term(service *sv);
int service_close_notif_fd(service *sv);
int service_close_out_fd(service *sv);
int service_close_fds(service *sv);
int service_transition_kill(service *sv);
int service_transition_exit(service *sv, int code);
int service_transition_stop_timeout(service *sv);
int service_transition_finish_timeout(service *sv);
int service_transition_wait_timeout(service *sv);
int service_transition_wait_deps(service *sv);
int service_start(service *sv);
int service_stop(service *sv);
int service_wakeup_time(service *sv, struct timespec *ts);
