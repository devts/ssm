#include "all.h"

configsearchpathitem configsearchpaths_user[] = {
    {1, "/lib/ssmcfg/"},
    {1, "/.config/ssmcfg/"},
    {0, "./ssmcfg/"},
};

configsearchpathitem configsearchpaths_root[] = {
    {0, "/etc/ssmcfg/"},
};

int
get_ssmcfg_path(uid_t uid, char **ssmcfg_path) {
    char *homedir = getenv("HOME");
    char *path;
    configsearchpathitem *cspi = uid ? configsearchpaths_user : configsearchpaths_root;
    size_t cspin = uid ? LEN(configsearchpaths_user) : LEN(configsearchpaths_root);
    for (size_t i = 0; i < cspin; i++) {
        char *dir = cspi[i].dir;
        path = cspi[i].reltohome ? pathmcat(homedir, dir) : estrdup(dir);
        if (!access(path, R_OK)) {
            *ssmcfg_path = path;
            return 0;
        }
        free(path);
    }
    return 1;
}
