#include "all.h"

serviceset *
serviceset_new(char *cfgpath, struct timespec startup) {
  serviceset *ss = ecalloc(1, sizeof(serviceset));
  ss->cfgpath = cfgpath ? estrdup(cfgpath) : 0;
  ss->startup = startup;
  return ss;
}

void
serviceset_delete(serviceset *ss, int closefds) {
  free(ss->cfgpath);
  free(ss->svs_rto);
  for (size_t i = 0; i < ss->nsvs; i++)
    service_delete(ss->svs[i], closefds);
  free(ss->svs);
  free(ss);
}

service *
serviceset_service_by_name(serviceset *ss, char *name) {
  if (!name)
    return 0;
  for (size_t i = 0; i < ss->nsvs; i++)
    if (!strcmp(ss->svs[i]->name, name))
      return ss->svs[i];
  return 0;
}

service *
serviceset_load_single(serviceset *ss, char *name) {
  if (!name)
    return 0;
  service *sv = serviceset_service_by_name(ss, name);
  if (sv)
    return sv;
  char *path = pathmcat(ss->cfgpath, name);
  sv = service_read(path, name);
  free(path);
  if (!sv)
    return 0;
  list_append((void *)&ss->svs, &ss->nsvs, &ss->msvs, sizeof(service *), &sv);
  return sv;
}

int
serviceset_make_service_deps(serviceset *ss, service *sv) {
  if (sv->ndeps == sv->ndep_names)
    return 0;
  sv->ndeps = 0;
  for (size_t i = 0; i < sv->ndep_names; i++) {
    service *dsv = serviceset_service_by_name(ss, sv->dep_names[i]);
    if (!dsv)
      return 1;
    list_append((void **)&sv->deps, &sv->ndeps, &sv->mdeps, sizeof(service *), &dsv);
  }
  return 0;
}

int
serviceset_service_load_deps(serviceset *ss, service *sv) {
  int r = 0;
  for (size_t i = 0; i < sv->ndep_names; i++)
    r |= !serviceset_load_single(ss, sv->dep_names[i]);
  return r;
}

int
serviceset_service2idx(serviceset *ss, service *sv, size_t *i) {
  char *v = 0;
  size_t j;
  list_find((void **)&ss->svs, &ss->nsvs, &ss->msvs, sizeof(service *), &sv, &v, &j);
  if (v)
    return *i = j, 0;
  else
    return 1;
}

int
visit(serviceset *ss, int *marks, size_t i) {
  if (marks[i] == 2)
    return 0;
  if (marks[i] == 1)
    return 1;
  marks[i] = 1;
  for (size_t j = 0; j < ss->svs[i]->ndeps; j++) {
    size_t k = 0;
    if (serviceset_service2idx(ss, ss->svs[i]->deps[j], &k))
      return 1;
    if (visit(ss, marks, k))
      return 1;
  }
  marks[i] = 2;
  list_append((void **)&ss->svs_rto, &ss->nsvs_rto, &ss->msvs_rto, sizeof(service *), ss->svs + i);
  return 0;
}

int
serviceset_dorto(serviceset *ss) {
  int *marks = ecalloc(ss->nsvs, sizeof(int)); /* 1 = tmark 2 = pmark */
  int r = 0;
  ss->nsvs_rto = 0;
  for (size_t i = 0; i < ss->nsvs; i++)
    if (!marks[i])
      r |= visit(ss, marks, i);
  free(marks);
  return r;
}

int
serviceset_make_relations(serviceset *ss) {
  int r;
  for (size_t i = 0; i < ss->nsvs; i++)
    if ((r = serviceset_make_service_deps(ss, ss->svs[i])))
      return r;
  return serviceset_dorto(ss);
}

int
serviceset_iterative_depload(serviceset *ss, size_t psvs) {
  int r = 0;
  while (psvs != ss->nsvs) {
    size_t ssvs = psvs;
    size_t esvs = ss->nsvs;
    for (size_t i = ssvs; i < esvs; i++)
      r |= serviceset_service_load_deps(ss, ss->svs[i]);
    psvs = esvs;
  }
  return r;
}

int
serviceset_load(serviceset *ss, char *name) {
  if (!name)
    return 1;
  size_t psvs = ss->nsvs;
  service *sv = serviceset_load_single(ss, name);
  if (!sv)
    return 1;
  if (serviceset_iterative_depload(ss, psvs))
    return 1;
  return serviceset_make_relations(ss);
}

int
serviceset_service_reread(service *sv) {
  service_reset_cfg(sv);
  return service_read_cfg(sv);
}

int
serviceset_reload(serviceset *ss, char *name) {
  if (name) {
    service *sv = serviceset_service_by_name(ss, name);
    if (!sv)
      return 1;
    if (serviceset_service_reread(sv))
      return 1;
  } else {
    for (size_t i = 0; i < ss->nsvs; i++)
      if (serviceset_service_reread(ss->svs[i]))
        return 1;
  }
  if (serviceset_iterative_depload(ss, 0))
    return 1;
  return serviceset_make_relations(ss);
}

serviceset *
serviceset_copy(serviceset *ss) {
  serviceset *sd = serviceset_new(ss->cfgpath, ss->startup);
  for (size_t i = 0; i < ss->nsvs; i++) {
    service *sv = service_copy(ss->svs[i]);
    list_append((void **)&sd->svs, &sd->nsvs, &sd->msvs, sizeof(service *), &sv);
  }
  if (serviceset_make_relations(sd))
    return serviceset_delete(sd, 0), (serviceset *)0;
  return sd;
}

void
serviceset_ser(serviceset *ss, char **buf, size_t *nbuf, size_t *mbuf) {
  ser_cstr(buf, nbuf, mbuf, "serviceset");
  ser_cstr(buf, nbuf, mbuf, ss->cfgpath);
  ser_timespec(buf, nbuf, mbuf, ss->startup);
  ser_sizet(buf, nbuf, mbuf, ss->nsvs);
  for (size_t i = 0; i < ss->nsvs; i++)
    service_ser(ss->svs[i], buf, nbuf, mbuf);
}

int
serviceset_deser(char *buf, size_t nbuf, size_t *of, serviceset **ssr) {
  serviceset *ss = serviceset_new(0, (struct timespec){.tv_sec = 0, .tv_nsec = 0});
  char *s = 0;
  size_t nsvs;
  service *sv;
  if (deser_cstr(buf, nbuf, of, &s))
    return service_delete(sv, 0), 1;
  if (strcmp(s, "serviceset"))
    return free(s), serviceset_delete(ss, 0), 1;
  free(s);
  if (deser_cstr(buf, nbuf, of, &ss->cfgpath))
    return serviceset_delete(ss, 0), 1;
  if (deser_timespec(buf, nbuf, of, &ss->startup))
    return serviceset_delete(ss, 0), 1;
  if (deser_sizet(buf, nbuf, of, &nsvs))
    return serviceset_delete(ss, 0), 1;
  for (size_t i = 0; i < nsvs; i++) {
    if (service_deser(buf, nbuf, of, &sv))
      return serviceset_delete(ss, 0), 1;
    list_append((void *)&ss->svs, &ss->nsvs, &ss->msvs, sizeof(service *), &sv);
  }
  if (serviceset_make_relations(ss))
    return serviceset_delete(ss, 0), 1;
  *ssr = ss;
  return 0;
}

int
serviceset_check_stop_timeout(serviceset *ss) {
  int r = 0;
  for (size_t i = 0; i < ss->nsvs_rto; i++)
    r |= service_transition_stop_timeout(ss->svs_rto[i]);
  return r;
}
int
serviceset_check_finish_timeout(serviceset *ss) {
  int r = 0;
  for (size_t i = 0; i < ss->nsvs_rto; i++)
    r |= service_transition_finish_timeout(ss->svs_rto[i]);
  return r;
}
int
serviceset_check_wait_timeout(serviceset *ss) {
  int r = 0;
  for (size_t i = 0; i < ss->nsvs_rto; i++)
    r |= service_transition_wait_timeout(ss->svs_rto[i]);
  return r;
}
int
serviceset_check_wait_deps(serviceset *ss) {
  int r = 0;
  for (size_t i = 0; i < ss->nsvs_rto; i++)
    r |= service_transition_wait_deps(ss->svs_rto[i]);
  return r;
}

void
serviceset_do_auto_transitions(serviceset *ss) {
  serviceset_check_stop_timeout(ss);
  serviceset_check_finish_timeout(ss);
  serviceset_check_wait_timeout(ss);
  serviceset_check_wait_deps(ss);
}

int
serviceset_start_service(serviceset *ss, service *sv, int level) {
  int r = 0;
  for (size_t i = 0; i < sv->ndeps; i++) 
    r |= serviceset_start_service(ss, sv->deps[i], level + 1);
  r |= service_start(sv);
  if (!level)
    serviceset_do_auto_transitions(ss);
  return r;
}

int
serviceset_handle_exit(serviceset *ss, pid_t pid, int code) {
  for (size_t i = 0; i < ss->nsvs_rto; i++)
    if (ss->svs_rto[i]->pid == pid)
      return service_transition_exit(ss->svs_rto[i], code);
  return 1;
}

int
serviceset_start(serviceset *ss, char *name) {
  if (!name)
    return 1;
  service *sv = serviceset_service_by_name(ss, name);
  if (!sv)
    return 1;
  return serviceset_start_service(ss, sv, 0);
}

int
serviceset_stop(serviceset *ss, char *name) {
  if (!name)
    return 1;
  service *sv = serviceset_service_by_name(ss, name);
  if (!sv)
    return 1;
  return service_stop(sv);
}

int
serviceset_restart(serviceset *ss, char *name) {
  return serviceset_stop(ss, name) || serviceset_start(ss, name);
}

int
serviceset_stop_all(serviceset *ss) {
  int r = 0;
  for (size_t i = 0; i < ss->nsvs_rto; i++)
    r |= service_stop(ss->svs_rto[ss->nsvs_rto - i - 1]);
  return r;
}

int
serviceset_all_stopped(serviceset *ss) {
  for (size_t i = 0; i < ss->nsvs_rto; i++)
    if (ss->svs_rto[i]->status != STOPPED && ss->svs_rto[i]->status != FAILED)
      return 1;
  return 0;
}

int
serviceset_notified(serviceset *ss, int fd, int revents) {
  for (size_t i = 0; i < ss->nsvs; i++)
    if (fd == ss->svs[i]->notif_fd_src)
      return service_fd_polled(ss->svs[i], revents);
  return 1;
}

void
serviceset_append_fds(serviceset *ss, struct pollfd **list, size_t *nlist, size_t *mlist) {
  for (size_t i = 0; i < ss->nsvs; i++) {
    if (ss->svs[i]->notif_fd_src >= 0) {
      struct pollfd pfd;
      pfd.fd = ss->svs[i]->notif_fd_src;
      pfd.events = POLLIN;
      pfd.revents = 0;
      list_append((void **)list, nlist, mlist, sizeof(struct pollfd), &pfd);
    }
  }
}

int
serviceset_wakeup_time(serviceset *ss, struct timespec *ts) {
  int r = 1;
  for (size_t i = 0; i < ss->nsvs; i++)
    r &= service_wakeup_time(ss->svs[i], ts);
  return r;
}

void
serviceset_print_info(serviceset *ss) {
  printf("ssmcfg path: %s\n", ss->cfgpath);
  printf("%-30s %-12s %-12s %12s %6s %8s %8s\n", "NAME", "STATUS", "TARGET", "PID", "DEAMON", "NOTIF_FD", "N_FD_SRC");
  for (size_t i = 0; i < ss->nsvs_rto; i++) {
    service *sv = ss->svs_rto[i];
    printf("%-30s %-12s %-12s %12ld %6s %8d %8d\n",
      sv->name,
      service_status_str(sv->status),
      service_status_str(sv->target),
      (long)sv->pid,
      sv->daemon ? "yes" : "no",
      sv->notif_fd,
      sv->notif_fd_src);
  }
}

void
print_timespec_nz(struct timespec ts) {
  if (ts.tv_sec == 0 && ts.tv_nsec == 0)
    printf("               ");
  else
    printf("%9ld.%04ld ", ts.tv_sec, ts.tv_nsec / 100000);
}

void
serviceset_print_timers(serviceset *ss) {
  printf("startup: %9ld.%04ld\n", ss->startup.tv_sec, ss->startup.tv_nsec / 100000);
  struct timespec now;
  clock_gettime(CLOCK_MONOTONIC, &now);
  printf("now:     %9ld.%04ld\n", now.tv_sec, now.tv_nsec / 100000);
  printf("%-30s %-12s %14s %14s %14s %6s %14s %14s\n",
      "NAME", "STATUS", "ACTIVE TIMER", "LAST RUN START",
      "LAST RUN STOP", "R EXIT", "L AVAIL START", "L AVAIL STOP");
  for (size_t i = 0; i < ss->nsvs_rto; i++) {
    service *sv = ss->svs_rto[i];
    printf("%-30s %-12s ", sv->name, service_status_str(sv->status));
    print_timespec_nz(
        sv->status == WAIT_TIMEOUT || sv->status == STOPPING || sv->status == FINISH ?
        sv->not_before : (struct timespec) {.tv_sec = 0, .tv_nsec = 0});
    print_timespec_nz(sv->last_run_start);
    print_timespec_nz(sv->last_run_stop);
    if (sv->last_run_start.tv_sec == 0 && sv->last_run_start.tv_nsec == 0)
      printf("       ");
    else
      printf("%6d ", sv->last_run_exit_status);
    print_timespec_nz(sv->last_available_start);
    print_timespec_nz(sv->last_available_stop);
    printf("\n");
  }
}
