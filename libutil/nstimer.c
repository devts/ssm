#include "../all.h"

nstimer *
nstimer_new(void) {
  nstimer *t = emalloc(sizeof(nstimer));
  t->t_ns = 0;
  return t;
}

void
nstimer_continue(nstimer *t) {
  clock_gettime(CLOCK_MONOTONIC, &t->start);
}

void
nstimer_start(nstimer *t) {
  t->t_ns = 0;
  nstimer_continue(t);
}

static uint64_t
nstimer_timespec_diff_ns(struct timespec *ts1, struct timespec *ts2) {
  return ((uint64_t)(ts2->tv_sec - ts1->tv_sec)*1000000000 + (uint64_t)ts2->tv_nsec) - (uint64_t)ts1->tv_nsec;
}

void
nstimer_stop(nstimer *t) {
  struct timespec now;
  clock_gettime(CLOCK_MONOTONIC, &now);
  t->t_ns += nstimer_timespec_diff_ns(&t->start, &now);
}

void
nstimer_delete(nstimer *t) {
  free(t);
}
