#include "../all.h"

int
fd_coe(int fd)
{
  int flags = fcntl(fd, F_GETFD, 0);
  return flags < 0 ? flags : flags & FD_CLOEXEC ? 0 : fcntl(fd, F_SETFD, flags | FD_CLOEXEC);
}

int
fd_nb(int fd)
{
  int got = fcntl(fd, F_GETFL);
  return got < 0 ? got : got & O_NONBLOCK ? 0 : fcntl(fd, F_SETFL, got | O_NONBLOCK);
}

int
fd_bl(int fd)
{
  int got = fcntl(fd, F_GETFL);
  return got < 0 ? got : got & O_NONBLOCK ? fcntl(fd, F_SETFL, got & ~O_NONBLOCK) : 0;
}

int
fd_dup2(int to, int from)
{
  int r;
  if (to == from)
    return 0;
  do
    r = dup2(from, to);
  while ((r == -1) && (errno == EINTR));
  if (r < 0)
    return r;
  return 0;
}

void
fd_close(int fd)
{
  int e = errno;
  while (close(fd) < 0 && errno == EINTR);
  errno = e;
}

int
fd_move(int to, int from)
{
  int r;
  if (to == from)
    return 0;
  r = fd_dup2(to, from);
  if (r < 0)
    return r;
  fd_close(from);
  return 0;
}

int
fd_lock(int fd, int w, int nb)
{
  struct flock fl =
  {
    .l_type = w ? F_WRLCK : F_RDLCK,
    .l_whence = SEEK_SET,
    .l_start = 0,
    .l_len = 0
  };
  int e = errno;
  int r;
  do r = fcntl(fd, nb ? F_SETLK : F_SETLKW, &fl);
  while (r == -1 && errno == EINTR);
  return r != -1 ? 1 :
    errno == EACCES || errno == EAGAIN || errno == EWOULDBLOCK ? (errno = e, 0) :
    -1;
}

void
fd_unlock(int fd)
{
  struct flock fl =
  {
    .l_type = F_UNLCK,
    .l_whence = SEEK_SET,
    .l_start = 0,
    .l_len = 0
  };
  int e = errno;
  fcntl(fd, F_SETLK, &fl);
  errno = e;
}

int
fd_islocked(int fd)
{
  struct flock fl =
  {
    .l_type = F_RDLCK,
    .l_whence = SEEK_SET,
    .l_start = 0,
    .l_len = 0
  };
  return fcntl(fd, F_GETLK, &fl) == -1 ? -1 : fl.l_type != F_UNLCK;
}

iores
fd_read(int fd, char *buf, size_t len) {
  ssize_t r;
  size_t of = 0;
  do {
    r = read(fd, buf + of, len - of);
    of = r > 0 ? of + r : of;
  } while ((r > 0 && of < len) || ((r == -1) && (errno == EINTR)));
  return (iores){.s = of, .eof = !r, .err = r < 0 ? errno : 0};
}

iores
fd_write(int fd, char *buf, size_t len) {
  ssize_t r;
  size_t of = 0;
  do {
    r = write(fd, buf + of, len - of);
    of = r > 0 ? of + r : of;
  } while ((r > 0 && of < len) || ((r == -1) && (errno == EINTR)));
  return (iores){.s = of, .eof = 0, .err = r < 0 ? errno : 0};
}

iores
fd_read_all(int fd, char **v, size_t *n, size_t *m) {
  size_t ns = *n;
  iores r = {.s = 0, .eof = 0, .err = 0};
  do {
    list_minsize((void **)v, n, m, 1, *n + 128);
    r = fd_read(fd, *v + *n, *m - *n);
    *n += r.s;
  } while (!r.eof && !r.err);
  r.s = *n - ns;
  return r;
}
