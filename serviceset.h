typedef struct {
  char *cfgpath;
  struct timespec startup;
  service **svs; /* services in load order */
  size_t nsvs;
  size_t msvs;
  service **svs_rto; /* service dag in reverse topological order */
  size_t nsvs_rto;
  size_t msvs_rto;
} serviceset;

serviceset *serviceset_new(char *cfgpath, struct timespec startup);
void serviceset_delete(serviceset *ss, int closefds);
serviceset *serviceset_copy(serviceset *ss);
void serviceset_ser(serviceset *ss, char **buf, size_t *nbuf, size_t *mbuf);
int serviceset_deser(char *buf, size_t nbuf, size_t *of, serviceset **ssr);
service *serviceset_service_by_name(serviceset *ss, char *name);
/* load and reload operations will leave serviceset in an inconsistent state
 * if they fail. Always perform these operations on a fresh copy to discard on
 * failure. */
int serviceset_load(serviceset *ss, char *name);
int serviceset_reload(serviceset *ss, char *name);
int serviceset_start(serviceset *ss, char *name);
int serviceset_stop(serviceset *ss, char *name);
int serviceset_stop_all(serviceset *ss);
int serviceset_all_stopped(serviceset *ss);
int serviceset_restart(serviceset *ss, char *name);
int serviceset_handle_exit(serviceset *ss, pid_t pid, int code);
void serviceset_do_auto_transitions(serviceset *ss);
int serviceset_notified(serviceset *ss, int fd, int revents);
void serviceset_append_fds(serviceset *ss, struct pollfd **list, size_t *nlist, size_t *mlist);
int serviceset_wakeup_time(serviceset *ss, struct timespec *ts);
void serviceset_print_info(serviceset *ss);
void serviceset_print_timers(serviceset *ss);
