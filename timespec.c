#include "all.h"

struct timespec
ts_min(struct timespec a, struct timespec b) {
  if (a.tv_sec < b.tv_sec
      || (a.tv_sec == b.tv_sec && a.tv_nsec < b.tv_nsec))
    return a;
  else
    return b;
}

struct timespec
ts_plus(struct timespec a, struct timespec b) {
  struct timespec c;
  c.tv_sec = a.tv_sec + b.tv_sec;
  c.tv_nsec = a.tv_nsec + b.tv_nsec;
  c.tv_sec += c.tv_nsec / 1000000000;
  c.tv_nsec %= 1000000000;
  if (c.tv_nsec < 0) {
    c.tv_sec -= 1;
    c.tv_nsec += 1000000000;
  }
  return c;
}

struct timespec
ts_minus(struct timespec a, struct timespec b) {
  b.tv_sec *= -1;
  b.tv_nsec *= -1;
  return ts_plus(a, b);
}

void
ts_add_ms(struct timespec *a, long ms) {
  a->tv_sec += ms / 1000;
  a->tv_nsec += (ms % 1000) * 1000000;
}

void
ts_add_ms_p(struct timespec *a, long ms) {
  if (ms < 0)
    a->tv_sec = LONG_MAX, a->tv_nsec = 999999999;
  else
    ts_add_ms(a, ms);
}
