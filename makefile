EXEC_SSM=ssm
EXEC_SSMCTL=ssmctl
DBGEXEC_SSM=ssmdbg
DBGEXEC_SSMCTL=ssmctldbg

all: $(EXEC_SSM) $(EXEC_SSMCTL)
debug: $(EXEC_SSM) $(EXEC_SSMCTL) $(DBGEXEC_SSM) $(DBGEXEC_SSMCTL)
srelease: $(EXEC_SSM) $(EXEC_SSMCTL)

FILES_COMMON =\
	service.c serviceset.c cfgpath.c sigutil.c \
	timespec.c fio.c mq.c ser.c log.c \
	libutil/ealloc.c libutil/estrdup.c libutil/strmcat.c libutil/mpath.c \
	libutil/strcasestr.c libutil/strlcat.c libutil/strlcpy.c \
	libutil/list_functions.c libutil/nstimer.c libutil/fd.c libutil/execf.c
FILES_SSM = ssm.c ssmcmd.c $(FILES_COMMON)
FILES_SSMCTL = ssmctl.c ssmctlcmd.c $(FILES_COMMON)

DESTDIR ?= $(HOME)
CPUTYPE ?= amd64
PREFIX ?= /bin/$(CPUTYPE)
MANPREFIX ?= /man

CC ?= cc
CPPFLAGS ?= -D_XOPEN_SOURCE=700 -D_GNU_SOURCE
COMMONCFLAGS ?= -std=c99 -Wall -pedantic -Werror-implicit-function-declaration -fcommon
CFLAGS ?= $(CPPFLAGS) $(COMMONCFLAGS) -O2
LIBS ?= 
LDFLAGS ?= -s $(LIBS)

DBGCFLAGS ?= $(CPPFLAGS) $(COMMONCFLAGS) -g -O0
DBGLDFLAGS ?= $(LIBS)

srelease: CC = musl-gcc
srelease: CFLAGS = -I../ncurses/include -L../ncurses/lib $(CPPFLAGS) $(COMMONCFLAGS) -O2
srelease: LDFLAGS = -static -s $(LIBS)

$(EXEC_SSM): $(FILES_SSM)
	$(CC) $(CFLAGS) $(FILES_SSM) $(LDFLAGS) -o $@

$(DBGEXEC_SSM): $(FILES_SSM)
	$(CC) $(DBGCFLAGS) $(FILES_SSM) $(DBGLDFLAGS) -o $@

$(EXEC_SSMCTL): $(FILES_SSMCTL)
	$(CC) $(CFLAGS) $(FILES_SSMCTL) $(LDFLAGS) -o $@

$(DBGEXEC_SSMCTL): $(FILES_SSMCTL)
	$(CC) $(DBGCFLAGS) $(FILES_SSMCTL) $(DBGLDFLAGS) -o $@

pdf: status.pdf
status.pdf: status.tex
	pdflatex --interaction=nonstopmode $<

clean:
	rm -f $(EXEC_SSM) $(EXEC_SSMCTL) $(DBGEXEC_SSM) $(DBGEXEC_SSMCTL)

install:
	mkdir -p $(DESTDIR)$(PREFIX)
	cp -f $(EXEC_SSM) $(DESTDIR)$(PREFIX)/$(EXEC_SSM)
	cp -f $(EXEC_SSMCTL) $(DESTDIR)$(PREFIX)/$(EXEC_SSMCTL)
	mkdir -p $(DESTDIR)$(MANPREFIX)/man1
	cp -f $(EXEC_SSM).1 $(DESTDIR)$(MANPREFIX)/man1/$(EXEC_SSM).1
	cp -f $(EXEC_SSMCTL).1 $(DESTDIR)$(MANPREFIX)/man1/$(EXEC_SSMCTL).1
	mkdir -p $(DESTDIR)$(MANPREFIX)/man5
	cp -f ssmcfg.5 $(DESTDIR)$(MANPREFIX)/man5/ssmcfg.5

.PHONY: clean install debug srelease
