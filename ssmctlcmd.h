typedef struct {
  char *cname;
  int (*cf)(char *);
} cmd;
cmd *cmds;
size_t ncmds;

int cmd_load(char *name);
int cmd_reload(char *name);
int cmd_start(char *name);
int cmd_stop(char *name);
int cmd_restart(char *name);
int cmd_term(char *ign);
int cmd_status(char *name);
int cmd_pid(char *name);
int cmd_list(char *ign);
int cmd_list_timers(char *ign);
int cmd_disable(char *name);
int cmd_enable(char *name);

void send_cmd(char *cmd, char *arg);
