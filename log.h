/* 0: critical 1: informational 2: debug */
int loglevel;

void logprintf(int loglvl, const char *fmt, ...);
void logfatal(int exitcode, int loglvl, const char *fmt, ...);
