#include "all.h"
#include "ssmctlcmd.h"
#include "ssmctl.h"

mq *in = 0;
mq *out = 0;
mq *evq = 0;

int doterm = 0;
int doint = 0;

void
onterm(int s) {
  doterm = 1;
}
void
onint(int s) {
  doint = 1;
}

sigset_t e;
int
mq_poll_recv(mq *q) {
  struct pollfd pfd;
  pfd.fd = q->fd;
  pfd.events = POLLIN;
  pfd.revents = 0;
  int r;
  do {
    r = ppoll(&pfd, 1, 0, &e);
  } while(r < 0 && errno == EINTR && !doterm && !doint);
  if (r && pfd.revents & POLLIN)
    mq_recv(q);
  return pfd.revents;
}

char *ssmcfg_path = 0;

void
ioinit(void) {
  char *inpath = pathmcat(ssmcfg_path, "ctlin");
  int fd_in = open(inpath, O_WRONLY);
  free(inpath);
  if (fd_in < 0)
    logfatal(1, 0, "could not open in pipe\n");
  fd_coe(fd_in);
  if (fd_lock(fd_in, 1, 0) <= 0)
    logfatal(1, 0, "could not aquire lock\n");
  in = mq_new(fd_in);

  char *outpath = pathmcat(ssmcfg_path, "ctlout");
  int fd_out = open(outpath, O_RDONLY | O_NONBLOCK);
  free(outpath);
  if (fd_out < 0)
    logfatal(1, 0, "could not open out pipe\n");
  fd_coe(fd_out);
  out = mq_new(fd_out);
}

int
main(int argc, char *argv[])
{
  size_t i;
  for (i = 1; i < (size_t)argc; i++) {
    if (!argv[i])
      logfatal(1, 0, "null argument");
    if (argv[i][0] != '-')
      break;
    if (!strcmp(argv[i], "-v")) {
      loglevel++;
    } else if (!strcmp(argv[i], "-q")) {
      loglevel--;
    } else if (!strcmp(argv[i], "-d")) {
      if (i + 1 >= argc)
        logfatal(1, 0, "not enough arguments");
      ssmcfg_path = estrdup(argv[++i]);
    } else {
      logfatal(1, 0, "unknown argument");
    }
  }
  if (i >= argc)
    logfatal(1, 0, "not enough arguments");

  if (!ssmcfg_path && get_ssmcfg_path(getuid(), &ssmcfg_path))
    logfatal(1, 0, "ssmcfg path not found or not accessible\n");
  if (!(!strcmp(argv[i], "enable") || !strcmp(argv[i], "disable")))
    ioinit();

  sigset_t sis;
  sigemptyset(&sis);
  sigaddset(&sis, SIGTERM);
  sigaddset(&sis, SIGINT);
  if (sigprocmask(SIG_SETMASK, &sis, 0))
    logfatal(1, 0, "sigprocmask failed\n");
  if (   sigsethandler(SIGINT,  onint )
      || sigsethandler(SIGTERM, onterm))
    logfatal(1, 0, "setting signal handlers failed\n");
  sigemptyset(&e);

  if (i == argc - 1) {
    send_cmd(argv[i], 0);
  } else {
    size_t j = i;
    for (i++; i < argc; i++)
      send_cmd(argv[j], argv[i]);
  }
  return 0;
}
