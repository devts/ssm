ssize_t mgetline(char **buff, size_t *n, FILE *f);
int rstrtol(const char *s, long *l);
int read_linearray(FILE *f, char ***argv, size_t *nargv, size_t *margv);
void freestrarray(char **deps, size_t ndeps, size_t mdeps);
int write_linearray(FILE *f, char **argv, size_t nargv, size_t margv);
int read_all_file(FILE *f, char **buf, size_t *nbuf, size_t *mbuf);
int read_long(FILE *f, long *l);
