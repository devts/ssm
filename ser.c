#include "all.h"

void
ser_cstr(char **buf, size_t *nbuf, size_t *mbuf, char *s) {
  char z = 0;
  if (s)
    list_append_range((void **)buf, nbuf, mbuf, 1, strlen(s) + 1, s);
  else
    list_append((void **)buf, nbuf, mbuf, 1, &z);
}

void
ser_long(char **buf, size_t *nbuf, size_t *mbuf, long l) {
  size_t n = snprintf(0, 0, "%ld", l);
  list_minsize((void **)buf, nbuf, mbuf, 1, *nbuf + n + 1);
  snprintf(*buf + *nbuf, n + 1, "%ld", l);
  *nbuf += n + 1;
}

void
ser_sizet(char **buf, size_t *nbuf, size_t *mbuf, size_t l) {
  size_t n = snprintf(0, 0, "%zu", l);
  list_minsize((void **)buf, nbuf, mbuf, 1, *nbuf + n + 1);
  snprintf(*buf + *nbuf, n + 1, "%zu", l);
  *nbuf += n + 1;
}

void
ser_cstrarray(char **buf, size_t *nbuf, size_t *mbuf, char **a, size_t n) {
  ser_sizet(buf, nbuf, mbuf, n);
  for (size_t i = 0; i < n; i++)
    ser_cstr(buf, nbuf, mbuf, a[i]);
}

void
ser_timespec(char **buf, size_t *nbuf, size_t *mbuf, struct timespec ts) {
  ser_long(buf, nbuf, mbuf, ts.tv_sec);
  ser_long(buf, nbuf, mbuf, ts.tv_nsec);
}

int
deser_cstr(char *buf, size_t nbuf, size_t *of, char **out) {
  size_t ofs = *of;
  size_t n = strnlen(buf + ofs, nbuf - ofs);
  *of += n;
  if (*of == nbuf)
    return *out = 0, 1;
  (*of)++;
  if (!n)
    return *out = 0, 0;
  *out = estrdup(buf + ofs);
  return 0;
}

int
deser_long(char *buf, size_t nbuf, size_t *of, long *out) {
  size_t ofs = *of;
  size_t n = strnlen(buf + ofs, nbuf - ofs);
  *of += n;
  if (*of == nbuf)
    return *out = 0, 1;
  (*of)++;
  if (sscanf(buf + ofs, "%ld", out) != 1)
    return *out = 0, 1;
  return 0;
}

int
deser_sizet(char *buf, size_t nbuf, size_t *of, size_t *out) {
  size_t ofs = *of;
  size_t n = strnlen(buf + ofs, nbuf - ofs);
  *of += n;
  if (*of == nbuf)
    return *out = 0, 1;
  (*of)++;
  if (sscanf(buf + ofs, "%zu", out) != 1)
    return *out = 0, 1;
  return 0;
}

int
deser_cstrarray(char *buf, size_t nbuf, size_t *of, char ***aout, size_t *nout, size_t *mout) {
  size_t n = 0;
  *aout = 0;
  *nout = 0;
  *mout = 0;
  if (deser_sizet(buf, nbuf, of, &n))
    return 1;
  for (size_t i = 0; i < n; i++) {
    char *s = 0;
    if (deser_cstr(buf, nbuf, of, &s))
      return 1;
    list_append((void **)aout, nout, mout, sizeof(char *), &s);
  }
  return 0;
}

int
deser_timespec(char *buf, size_t nbuf, size_t *of, struct timespec *out) {
  return deser_long(buf, nbuf, of, &out->tv_sec) ||
    deser_long(buf, nbuf, of, &out->tv_nsec);
}
